import { Component, OnInit } from '@angular/core';
declare var draw2d: any;
declare var $: any;

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit {
  connections = 150;
  timeToRender = 0;

  constructor() {}

  ngOnInit(): void {
    this.createNodes();
  }

  createNodes() {
    let start = performance.now();
    let nodesArray = Array(this.connections);
    nodesArray.fill(0);
    let jsonDocument: any[] = [];
    let y = -50;
    nodesArray.forEach((a, i) => {
      let offset = i % 4;
      let x = 330 * offset;
      if (offset === 0) {
        y += 60;
      }

      jsonDocument.push({
        type: 'draw2d.shape.node.Start',
        id: '' + i + '-start',
        x: x,
        y: y,
        width: 50,
        height: 50,
        radius: 2,
      });

      jsonDocument.push({
        type: 'draw2d.shape.node.End',
        id: '' + i + '-end',
        x: 272 + x,
        y: y,
        width: 50,
        height: 50,
        radius: 2,
      });

      jsonDocument.push({
        type: 'draw2d.Connection',
        id: '' + i + '-connection',
        radius: 10,
        stroke: 3,
        source: {
          node: '' + i + '-start',
          port: 'output0',
          decoration: 'draw2d.decoration.connection.CircleDecorator',
        },
        target: {
          node: '' + i + '-end',
          port: 'input0',
          decoration: 'draw2d.decoration.connection.ArrowDecorator',
        },
        vertex: [],
      });
    });


    let canvas = new draw2d.Canvas('gfx_holder');
    var reader = new draw2d.io.json.Reader();
    reader.unmarshal(canvas, jsonDocument);
    let stop = performance.now();
    this.timeToRender = Math.round(stop - start);

    // draw2d.Canvas.ex

  }
}

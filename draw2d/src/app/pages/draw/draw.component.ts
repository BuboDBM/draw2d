import { Component, OnInit } from '@angular/core';
declare var draw2d: any;
declare var $: any;

@Component({
  selector: 'app-draw',
  templateUrl: './draw.component.html',
  styleUrls: ['./draw.component.scss'],
})
export class DrawComponent implements OnInit {
  constructor() {}
  numberOfCircles = 1000;

  ngOnInit(): void {
    var jsonDocument = [
      {
        type: 'draw2d.shape.node.Start',
        id: '354fa3b9-a834-0221-2009-abc2d6bd852a',
        x: 25,
        y: 97,
        width: 50,
        height: 50,
        radius: 2,
      },
      {
        type: 'draw2d.shape.node.End',
        id: 'ebfb35bb-5767-8155-c804-14bda7759dc2',
        x: 272,
        y: 45,
        width: 50,
        height: 50,
        radius: 2,
      },
      {
        type: 'draw2d.Connection',
        id: '74ce9e7e-5f0e-8642-6bec-4ff9c54b3f0a',
        source: {
          node: '354fa3b9-a834-0221-2009-abc2d6bd852a',
          port: 'output0',
        },
        target: {
          node: 'ebfb35bb-5767-8155-c804-14bda7759dc2',
          port: 'input0',
        },
      },
    ];

    $('button').click(function () {
      alert('Jquery is on!');
    });

    let circles = new Array(this.numberOfCircles).fill(null).map((item, i) => ({
      name: i,
      x: 40 + i * this.generateRandomInteger(1, 5),
      y: 10 + i * this.generateRandomInteger(1, 6),
    }));

    let canvas = new draw2d.Canvas('gfx_holder');
    // only required on codepen...
    // canvas.setScrollArea(window);

    circles.forEach((circle) => {
      let shape = new draw2d.shape.basic.Circle({
        x: circle.x,
        y: circle.y,
        stroke: 3,
        color: '#3d3d3d',
        bgColor: '#3dff3d',
      });
      canvas.add(shape);
    });

    var reader = new draw2d.io.json.Reader();
    reader.unmarshal(canvas, jsonDocument);

    // this.displayJSON(canvas);

    // let shape = new draw2d.shape.basic.Circle({
    //   x: 40,
    //   y: 10,
    //   stroke: 3,
    //   color: '#3d3d3d',
    //   bgColor: '#3dff3d',
    // });
    // canvas.add(shape);
  }

  generateRandomInteger(min: number, max: number) {
    return Math.floor(min + Math.random() * (max + 1 - min));
  }

  generateConnections() {
    var connections = 10;

    let a = Array(connections);
    a.fill(0);
    let jsonDocument = [];
    let y = -50;
    a.forEach((a, i) => {
      let offset = i % 4;
      let x = 330 * offset;
      if (offset === 0) {
        y += 60;
      }

      jsonDocument.push({
        type: 'draw2d.shape.node.Start',
        id: '' + i + '-start',
        x: x,
        y: y,
        width: 50,
        height: 50,
        radius: 2,
      });
      jsonDocument.push({
        type: 'draw2d.shape.node.End',
        id: '' + i + '-end',
        x: 272 + x,
        y: y,
        width: 50,
        height: 50,
        radius: 2,
      });
      jsonDocument.push({
        type: 'MyConnection',
        id: '' + i + '-connection',
        radius: 10,
        stroke: 3,
        source: {
          node: '' + i + '-start',
          port: 'output0',
          decoration: 'draw2d.decoration.connection.CircleDecorator',
        },
        target: {
          node: '' + i + '-end',
          port: 'input0',
          decoration: 'draw2d.decoration.connection.ArrowDecorator',
        },
        vertex: [],
      });
    });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DrawComponent } from './pages/draw/draw.component';
import { EditorComponent } from './pages/editor/editor.component';

const routes: Routes = [
  { path: 'start', component: DrawComponent },
  { path: 'editor', component: EditorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
